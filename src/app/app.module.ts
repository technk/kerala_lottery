import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {BoxCreatorPage} from '../pages/box-creator/box-creator';
import {FourBoxCreaterPage} from '../pages/four-box-creater/four-box-creater';
import {FiveBoxCreaterPage} from '../pages/five-box-creater/five-box-creater';
import { SocialSharing } from '@ionic-native/social-sharing';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    BoxCreatorPage,
    FourBoxCreaterPage,
    FiveBoxCreaterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    BoxCreatorPage,
    FourBoxCreaterPage,
    FiveBoxCreaterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,SocialSharing,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
