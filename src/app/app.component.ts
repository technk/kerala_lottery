import { Component } from '@angular/core';
import { Platform,AlertController,App  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public alertCtrl: AlertController,
    public app: App,
    ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    platform.registerBackButtonAction(() => {
      // Catches the active view
      let nav = this.app.getActiveNavs()[0];
      let activeView = nav.getActive();                
      // Checks if can go back before show up the alert
      
          if (nav.canGoBack()){
              nav.pop();
          } else {
            if(activeView.name === 'HomePage') {
              const alert = this.alertCtrl.create({
                  title: 'Exit',
                  message: 'Are you sure you want to exit?',
                  buttons: [{
                      text: 'No',
                      role: 'cancel',
                      handler: () => {                        
                        console.log('** Saída do App Cancelada! **');
                      }
                  },{
                      text: 'Yes',
                      handler: () => {                        
                        platform.exitApp();
                      }
                  }]
              });
              alert.present();

            }
          }
  });



  }
}

