import { Component } from '@angular/core';
import { NavController,Platform } from 'ionic-angular';
import {BoxCreatorPage} from '../box-creator/box-creator'
import {FourBoxCreaterPage} from '../four-box-creater/four-box-creater';
import {FiveBoxCreaterPage} from '../five-box-creater/five-box-creater';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,public platform: Platform) {

  }

  boxTCreator(){
    this.navCtrl.push(BoxCreatorPage);
  }
  boxFoCreator(){
    this.navCtrl.push(FourBoxCreaterPage);
  }
  boxFiCreator(){
    this.navCtrl.push(FiveBoxCreaterPage);
  }

}
