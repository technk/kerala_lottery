import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FourBoxCreaterPage } from './four-box-creater';

@NgModule({
  declarations: [
    FourBoxCreaterPage,
  ],
  imports: [
    IonicPageModule.forChild(FourBoxCreaterPage),
  ],
})
export class FourBoxCreaterPageModule {}
