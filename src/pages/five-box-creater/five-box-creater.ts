import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup,FormArray } from '@angular/forms';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the FiveBoxCreaterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-five-box-creater',
  templateUrl: 'five-box-creater.html',
})
export class FiveBoxCreaterPage {

  todo:any;
  tempStore:any = [];
  a: FormArray;
  b: FormArray;
  c: FormArray;
  d: FormArray;
  e: FormArray;

  constructor(public navCtrl: NavController,private socialSharing: SocialSharing, public navParams: NavParams,private formBuilder: FormBuilder ) {

    this.todo = this.formBuilder.group({      
      a: this.formBuilder.array([ this.createItem(),this.createItem(),this.createItem() ]),
      b: this.formBuilder.array([ this.createItem(),this.createItem(),this.createItem() ]),
      c: this.formBuilder.array([ this.createItem(),this.createItem(),this.createItem() ]),
      d: this.formBuilder.array([ this.createItem(),this.createItem(),this.createItem() ]),
      e: this.formBuilder.array([ this.createItem(),this.createItem(),this.createItem() ]),
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      name: '',
    });
  }

  addAItem(): void {
    this.a = this.todo.get('a') as FormArray;
    if(this.a.value.length < 10){
      this.a.push(this.createItem());
    }
  }
  addBItem(): void {
    this.b = this.todo.get('b') as FormArray;
    if(this.b.value.length < 10){
      this.b.push(this.createItem());
    }
  }
  addCItem(): void {
    this.c = this.todo.get('c') as FormArray;
    if(this.c.value.length < 10){
      this.c.push(this.createItem());
    }
  }
  addDItem(): void {
    this.d = this.todo.get('d') as FormArray;
    if(this.d.value.length < 10){
      this.d.push(this.createItem());
    }
  }
  addEItem(): void {
    this.e = this.todo.get('e') as FormArray;
    if(this.e.value.length < 10){
      this.e.push(this.createItem());
    }
  }

  boxTCreator() {        
    
    this.a = this.todo.get('a') as FormArray;
    this.b = this.todo.get('b') as FormArray;
    this.c = this.todo.get('c') as FormArray;
    this.d = this.todo.get('d') as FormArray;
    this.e = this.todo.get('e') as FormArray;
    this.tempStore = [];
    for (let index = 0; index < this.a.value.length; index++) {
      const aelement = this.a.value[index];      
      for (let bIndex = 0; bIndex < this.b.value.length; bIndex++) {
        const belement = this.b.value[bIndex];
        for (let cIndex = 0; cIndex < this.c.value.length; cIndex++) {
          const celement = this.c.value[cIndex];
          for (let dIndex = 0; dIndex < this.c.value.length; dIndex++) {
            const delement = this.c.value[dIndex];
          for (let eIndex = 0; eIndex < this.c.value.length; eIndex++) {
            const eelement = this.c.value[eIndex];
            if(aelement.name != null && aelement.name != "" && belement.name != null && belement.name != "" && celement.name != null && celement.name != "" && delement.name != null && delement.name != "" && eelement.name != null && eelement.name != ""){
              this.tempStore.push(aelement.name + belement.name+celement.name + delement.name + eelement.name);
            }
          }
          }
        }
      }
      
    }
    
  }

  ShareSocial() {        
    this.socialSharing.share(this.tempStore.join(),null,null,null).then(() => {
      // Sharing via email is possible        
    }).catch(() => {
      // Sharing via email is not possible
    });
  }
}
