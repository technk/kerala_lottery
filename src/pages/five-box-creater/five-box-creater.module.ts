import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiveBoxCreaterPage } from './five-box-creater';

@NgModule({
  declarations: [
    FiveBoxCreaterPage,
  ],
  imports: [
    IonicPageModule.forChild(FiveBoxCreaterPage),
  ],
})
export class FiveBoxCreaterPageModule {}
