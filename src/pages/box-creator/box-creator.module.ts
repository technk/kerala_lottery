import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BoxCreatorPage } from './box-creator';

@NgModule({
  declarations: [
    BoxCreatorPage,
  ],
  imports: [
    IonicPageModule.forChild(BoxCreatorPage),
  ],
})
export class BoxCreatorPageModule {}
